
package ee.ttu.idu0075.iapb155619.ws.competition;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for competitionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="competitionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="competitionName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="competitionDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="registrationDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="competitionRunnerList" type="{http://www.ttu.ee/idu0075/iapb155619/ws/competition}competitionRunnerListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "competitionType", propOrder = {
    "id",
    "competitionName",
    "competitionDate",
    "registrationDate",
    "competitionRunnerList"
})
public class CompetitionType {

    @XmlElement(required = true)
    protected BigInteger id;
    @XmlElement(required = true)
    protected String competitionName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar competitionDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar registrationDate;
    @XmlElement(required = true)
    protected CompetitionRunnerListType competitionRunnerList;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setId(BigInteger value) {
        this.id = value;
    }

    /**
     * Gets the value of the competitionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompetitionName() {
        return competitionName;
    }

    /**
     * Sets the value of the competitionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompetitionName(String value) {
        this.competitionName = value;
    }

    /**
     * Gets the value of the competitionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCompetitionDate() {
        return competitionDate;
    }

    /**
     * Sets the value of the competitionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCompetitionDate(XMLGregorianCalendar value) {
        this.competitionDate = value;
    }

    /**
     * Gets the value of the registrationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRegistrationDate() {
        return registrationDate;
    }

    /**
     * Sets the value of the registrationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRegistrationDate(XMLGregorianCalendar value) {
        this.registrationDate = value;
    }

    /**
     * Gets the value of the competitionRunnerList property.
     * 
     * @return
     *     possible object is
     *     {@link CompetitionRunnerListType }
     *     
     */
    public CompetitionRunnerListType getCompetitionRunnerList() {
        return competitionRunnerList;
    }

    /**
     * Sets the value of the competitionRunnerList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompetitionRunnerListType }
     *     
     */
    public void setCompetitionRunnerList(CompetitionRunnerListType value) {
        this.competitionRunnerList = value;
    }

}
