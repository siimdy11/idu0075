
package ee.ttu.idu0075.iapb155619.ws.competition;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for runnerSearchType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="runnerSearchType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="fistname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="lastname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="gender" type="{http://www.ttu.ee/idu0075/iapb155619/ws/competition}genderType" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "runnerSearchType", propOrder = {

})
public class RunnerSearchType {

    protected String fistname;
    protected String lastname;
    @XmlSchemaType(name = "string")
    protected GenderType gender;

    /**
     * Gets the value of the fistname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFistname() {
        return fistname;
    }

    /**
     * Sets the value of the fistname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFistname(String value) {
        this.fistname = value;
    }

    /**
     * Gets the value of the lastname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Sets the value of the lastname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastname(String value) {
        this.lastname = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link GenderType }
     *     
     */
    public GenderType getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenderType }
     *     
     */
    public void setGender(GenderType value) {
        this.gender = value;
    }
    
    public List<SearchType> getSearchParams() {
        List<SearchType> list = new ArrayList<>();
        if (fistname != null) list.add(new SearchType(fistname, "firstname"));
        if (lastname != null) list.add(new SearchType(lastname, "lastname"));
        if (gender != null) list.add(new SearchType(gender.value(), "gender"));
        return list;
    }

}
