package ee.ttu.idu0075.iapb155619.ws.competition;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Siim
 */
public class SearchType {
    private String columnItem;
    private String columnName;
    private Long columnItem2;

    public SearchType(String columnItem, String columnName) {
        this.columnItem = columnItem;
        this.columnName = columnName;
    }
    
    public SearchType(int columnItem, String columnName) {
        this.columnItem2 = (long) columnItem;
        this.columnName = columnName;
    }
    
    public SearchType(Long columnItem, String columnName) {
        this.columnItem2 = columnItem;
        this.columnName = columnName;
    }

    public String getColumnItem() {
        return columnItem;
    }
    
    public Long getColumnItem2() {
        return columnItem2;
    }

    public String getColumnName() {
        return columnName;
    }
    
    public boolean hasString() {
        return columnItem != null;
    }
    
    public String getSqlSearchString() {
//        if (columnItem == null) {
//            return columnName;
//        }
        return columnName + " = ?";
    }
}
