
package ee.ttu.idu0075.iapb155619.ws.competition;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for competitionRunnerType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="competitionRunnerType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="runner" type="{http://www.ttu.ee/idu0075/iapb155619/ws/competition}runnerType"/&gt;
 *         &lt;element name="group" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "competitionRunnerType", propOrder = {
    "runner",
    "group",
    "number"
})
@XmlRootElement
public class CompetitionRunnerType {

    @XmlElement(required = true)
    protected RunnerType runner;
    @XmlElement(required = true)
    protected String group;
    @XmlElement(required = true)
    protected BigInteger number;

    /**
     * Gets the value of the runner property.
     * 
     * @return
     *     possible object is
     *     {@link RunnerType }
     *     
     */
    public RunnerType getRunner() {
        return runner;
    }

    /**
     * Sets the value of the runner property.
     * 
     * @param value
     *     allowed object is
     *     {@link RunnerType }
     *     
     */
    public void setRunner(RunnerType value) {
        this.runner = value;
    }

    /**
     * Gets the value of the group property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroup() {
        return group;
    }

    /**
     * Sets the value of the group property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroup(String value) {
        this.group = value;
    }

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumber(BigInteger value) {
        this.number = value;
    }

}
