/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.database;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import ee.ttu.idu0075.competition.CompetitionWebService;
import ee.ttu.idu0075.iapb155619.ws.competition.CompetitionRunnerListType;
import ee.ttu.idu0075.iapb155619.ws.competition.CompetitionRunnerSearchType;
import ee.ttu.idu0075.iapb155619.ws.competition.CompetitionRunnerType;
import ee.ttu.idu0075.iapb155619.ws.competition.CompetitionSearchType;
import ee.ttu.idu0075.iapb155619.ws.competition.CompetitionType;
import ee.ttu.idu0075.iapb155619.ws.competition.GenderType;
import ee.ttu.idu0075.iapb155619.ws.competition.GetCompetitionListResponse;
import ee.ttu.idu0075.iapb155619.ws.competition.GetRunnerListResponse;
import ee.ttu.idu0075.iapb155619.ws.competition.RunnerSearchType;
import ee.ttu.idu0075.iapb155619.ws.competition.RunnerType;
import ee.ttu.idu0075.iapb155619.ws.competition.SearchType;
import java.io.StringReader;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.StringWriter;
import java.util.List;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;


/**
 *
 * @author Siim
 */
public class Database {
    
    public static void main(String[] args) {
    }

    public static Connection getConnection() {
        Connection conn = null;
        try {
            
            Class.forName("org.sqlite.JDBC");
            String url = "jdbc:sqlite:C:/sqlite/db/competition2.db";
            conn = DriverManager.getConnection(url);
            
            System.out.println("Connection to SQLite has been established.");
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return conn;
    }
    
    public static void closeConnection(Connection conn) {
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    public static RunnerType addRunner(RunnerType runner, String requestID) {
        String sql1 = "INSERT INTO runner (firstname, lastname, gender, birthdate, email, phone)"
                + "VALUES (?, ?, ?, ?, ?, ?)";

        try (Connection conn = Database.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql1)){

            pstmt.setString(1, runner.getFirstname());
            pstmt.setString(2, runner.getLastname());
            pstmt.setString(3, runner.getGender().value());
            pstmt.setString(4, runner.getBirthdate().toString());
            pstmt.setString(5, runner.getEmail());
            pstmt.setString(6, runner.getPhone());
            pstmt.executeUpdate();
            
            runner.setId(getLastId(conn));

            saveResponse(runner, requestID, conn);
            
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return runner;
    }
    
    public static RunnerType getRunner(long rID, String requestID) {
        String sql1 = "SELECT * FROM runner WHERE id = ?";

        try (Connection conn = Database.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql1)){

            pstmt.setLong(1, rID);
            ResultSet rs = pstmt.executeQuery();
            
            RunnerType runnerType = getRunnerFromResultSet(rs);

            saveResponse(runnerType, requestID, conn);
            
            return runnerType;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public static RunnerType getRunnerWithID(long rID) {
        String sql1 = "SELECT * FROM runner WHERE id = ?";

        try (Connection conn = Database.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql1)){

            pstmt.setLong(1, rID);
            ResultSet rs = pstmt.executeQuery();
            
            RunnerType runnerType = getRunnerFromResultSet(rs);

            return runnerType;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public static CompetitionType addCompetition(CompetitionType competition, String requestID) {
        String sql1 = "INSERT INTO competition (competition_name, competition_date, registration_date)"
                + "VALUES (?, ?, ?)";

        try (Connection conn = Database.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql1)){

            pstmt.setString(1, competition.getCompetitionName());
            pstmt.setString(2, competition.getCompetitionDate().toString());
            pstmt.setString(3, competition.getRegistrationDate().toString());
            pstmt.executeUpdate();
            
            competition.setId(getLastId(conn));

            saveResponse(competition, requestID, conn);
            
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return competition;
    }
    
    public static GetRunnerListResponse getRunnerListResponse(RunnerSearchType rst, String requestID) {
        String sql = "SELECT * FROM runner";
        
        List<SearchType> searchList = null;
        if (rst != null) searchList = rst.getSearchParams();
        if (searchList != null && !searchList.isEmpty()) {
            sql += " WHERE ";
            sql += searchList.stream().map(SearchType::getSqlSearchString).collect(Collectors.joining(" and "));
        }
        
        System.out.println(sql);
        
        try (Connection conn = Database.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql);){
            
            if (searchList != null) {
                for (int i = 0; i < searchList.size(); i++) {
                    pstmt.setString(i + 1, searchList.get(i).getColumnItem());
                }
            }
            ResultSet rs = pstmt.executeQuery();
            
            GetRunnerListResponse runnerListResponse = new GetRunnerListResponse();
            while (rs.next()) {
                runnerListResponse.getRunner().add(getRunnerFromResultSet(rs));
            }
            
            saveResponse(runnerListResponse, requestID, conn);
            
            return runnerListResponse;
            
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static GetCompetitionListResponse getCompetitionListResponse(CompetitionSearchType cst, String requestID) {
        String sql = "SELECT * FROM competition";
        
        List<SearchType> searchList = null;
        if (cst != null) searchList = cst.getSearchParams();
        if (searchList != null && !searchList.isEmpty()) {
            sql += " WHERE ";
            sql += searchList.stream().map(SearchType::getSqlSearchString).collect(Collectors.joining(" and "));
        }
        
        System.out.println(sql);
        
        try (Connection conn = Database.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql);){
            
            if (searchList != null) {
                int index = 1;
                for (int i = 0; i < searchList.size(); i++) {
                    if (searchList.get(i).hasString())
                        pstmt.setString(index++, searchList.get(i).getColumnItem());
                    else
                        pstmt.setLong(index++, searchList.get(i).getColumnItem2());
                }
            }
            ResultSet rs = pstmt.executeQuery();
            
            GetCompetitionListResponse competitionListResponse = new GetCompetitionListResponse();
            while (rs.next()) {
                competitionListResponse.getCompetition().add(getCompetitionFromResultSet(rs));
            }
            
            saveResponse(competitionListResponse, requestID, conn);
            
            return competitionListResponse;
            
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static CompetitionRunnerListType getCompetitionRunnerListResponse(Long cID, CompetitionRunnerSearchType rst, String requestID) {
//        String sql = "SELECT * FROM runner WHERE competition_id = ?";
        String sql = "SELECT r.id as id, r.firstname as firstname, r.lastname as lastname, r.gender as gender, "
                + "r.birthdate as birthdate, r.email as email, r.phone as phone, "
                + "cr.runner_group as runner_group, cr.runner_number as runner_number "
                + "FROM competition_runner AS cr "
                + "INNER JOIN runner AS r ON (r.id = cr.runner_id) "
                + "WHERE cr.competition_id = ?";
        
        List<SearchType> searchList = null;
        if (rst != null) searchList = rst.getSearchParams();
        if (searchList != null && !searchList.isEmpty()) {
            sql += " and ";
            sql += searchList.stream().map(SearchType::getSqlSearchString).collect(Collectors.joining(" and "));
        }
        
        System.out.println(sql);
        
        try (Connection conn = Database.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql);){
            
            pstmt.setLong(1, cID);
            
            if (searchList != null) {
                int index = 2;
                for (int i = 0; i < searchList.size(); i++) {
                    if (searchList.get(i).hasString())
                        pstmt.setString(index++, searchList.get(i).getColumnItem());
                    else
                        pstmt.setLong(index++, searchList.get(i).getColumnItem2());
                }
            }
            ResultSet rs = pstmt.executeQuery();
            
            CompetitionRunnerListType competitionRunnerListType = new CompetitionRunnerListType();
            while (rs.next()) {
                competitionRunnerListType.getCompetitionRunner().add(getCompetitionRunnerFromResultSet(rs));
            }
            
            saveResponse(competitionRunnerListType, requestID, conn);
            
            return competitionRunnerListType;
            
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }    

    public static CompetitionRunnerListType getCompetitionRunnerListWithID(long cID) {
        String sql = "SELECT r.id as id, r.firstname as firstname, r.lastname as lastname, r.gender as gender, "
                + "r.birthdate as birthdate, r.email as email, r.phone as phone, "
                + "cr.runner_group as runner_group, cr.runner_number as runner_number "
                + "FROM competition_runner AS cr "
                + "INNER JOIN runner AS r ON (r.id = cr.runner_id) "
                + "WHERE cr.competition_id = ?";
        
        System.out.println(sql);
        
        try (Connection conn = Database.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql);){
            
            pstmt.setLong(1, cID);
            
            ResultSet rs = pstmt.executeQuery();
            
            CompetitionRunnerListType competitionRunnerListType = new CompetitionRunnerListType();
            while (rs.next()) {
                competitionRunnerListType.getCompetitionRunner().add(getCompetitionRunnerFromResultSet(rs));
            }
            
            return competitionRunnerListType;
            
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    } 
    
    public static RunnerType getRunnerFromResultSet(ResultSet rs) {
        try {
            RunnerType rt = new RunnerType();
            rt.setFirstname(rs.getString("firstname"));
            rt.setLastname(rs.getString("lastname"));
            rt.setGender(GenderType.valueOf(rs.getString("gender")));
            rt.setBirthdate(XMLGregorianCalendarImpl.parse(rs.getString("birthdate")));
            rt.setEmail(rs.getString("email"));
            rt.setPhone(rs.getString("phone"));
            rt.setId(BigInteger.valueOf(rs.getLong("id")));
            return rt;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static CompetitionRunnerType getCompetitionRunnerFromResultSet(ResultSet rs) {
        try {
            CompetitionRunnerType crt = new CompetitionRunnerType();
            crt.setRunner(getRunnerFromResultSet(rs));
            crt.setGroup(rs.getString("runner_group"));
            crt.setNumber(BigInteger.valueOf(rs.getLong("runner_number")));
            return crt;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static CompetitionType getCompetitionFromResultSet(ResultSet rs) {
        try {
            CompetitionType ct = new CompetitionType();
            ct.setCompetitionName(rs.getString("competition_name"));
            ct.setCompetitionDate(XMLGregorianCalendarImpl.parse(rs.getString("competition_date")));
            ct.setRegistrationDate(XMLGregorianCalendarImpl.parse(rs.getString("registration_date")));
            long cID = rs.getLong("id");
            ct.setCompetitionRunnerList(Database.getCompetitionRunnerListWithID(cID));
            ct.setId(BigInteger.valueOf(cID));
            return ct;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static BigInteger getLastId(Connection conn) {
        String sql = "SELECT last_insert_rowid()";
        try (Statement stmt  = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)){

            return BigInteger.valueOf(rs.getLong(1));
            
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static <T> String saveResponse(T object, String requestID, Connection conn) {
        String sql = "INSERT INTO request_history(request_id, response)"
                    + "VALUES (?, ?)";
        
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(object, sw);
            String xmlString = sw.toString();
            
            try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
                pstmt.setString(1, requestID);
                pstmt.setString(2, xmlString);
                pstmt.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (JAXBException ex) {
            Logger.getLogger(CompetitionWebService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static String getResponse(String requestID) {
        String sql = "SELECT response FROM request_history "
                + "WHERE request_id = ?";
        try (Connection conn = Database.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            
            pstmt.setString(1, requestID);
            
            ResultSet rs = pstmt.executeQuery();
            
            if (rs.isBeforeFirst()) {
                return rs.getString("response");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public static CompetitionType getCompetition(long cID, String requestID) {
        String sql1 = "SELECT * FROM competition WHERE id = ?";

        try (Connection conn = Database.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql1)){

            pstmt.setLong(1, cID);
            ResultSet rs = pstmt.executeQuery();
            
            CompetitionType competitionType = getCompetitionFromResultSet(rs);

            saveResponse(competitionType, requestID, conn);
            
            return competitionType;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public static CompetitionRunnerType addCompetitionRunner(CompetitionRunnerType crt, String requestID, BigInteger cID, BigInteger rID) {
        String sql1 = "INSERT INTO competition_runner "
                + "(competition_id, runner_id, runner_group, runner_number) "
                + "VALUES (?, ?, ?, ?)";

        try (Connection conn = Database.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql1)){

            pstmt.setLong(1, cID.longValue());
            pstmt.setLong(2, rID.longValue());
            pstmt.setString(3, crt.getGroup());
            pstmt.setLong(4, crt.getNumber().longValue());
            pstmt.executeUpdate();
            
            crt.setRunner(getRunnerWithID(rID.longValue()));
            
            saveResponse(crt, requestID, conn);
            
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return crt;
    }
}
