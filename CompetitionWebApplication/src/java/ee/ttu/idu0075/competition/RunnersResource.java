/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.competition;

import ee.ttu.idu0075.iapb155619.ws.competition.AddRunnerRequest;
import ee.ttu.idu0075.iapb155619.ws.competition.CompetitionType;
import ee.ttu.idu0075.iapb155619.ws.competition.GenderType;
import ee.ttu.idu0075.iapb155619.ws.competition.GetCompetitionListRequest;
import ee.ttu.idu0075.iapb155619.ws.competition.GetCompetitionListResponse;
import ee.ttu.idu0075.iapb155619.ws.competition.GetCompetitionRequest;
import ee.ttu.idu0075.iapb155619.ws.competition.GetRunnerListRequest;
import ee.ttu.idu0075.iapb155619.ws.competition.GetRunnerListResponse;
import ee.ttu.idu0075.iapb155619.ws.competition.GetRunnerRequest;
import ee.ttu.idu0075.iapb155619.ws.competition.RunnerSearchType;
import ee.ttu.idu0075.iapb155619.ws.competition.RunnerType;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConstants;

/**
 * REST Web Service
 *
 * @author Siim
 */
@Path("runners")
public class RunnersResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of RunnersResource
     */
    public RunnersResource() {
    }

    /**
     * Retrieves representation of an instance of ee.ttu.idu0075.competition.RunnersResource
     * @return an instance of ee.ttu.idu0075.iapb155619.ws.competition.RunnerType
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public GetRunnerListResponse getRunnerList(@QueryParam("token") String token,
            @QueryParam("rid") String requestID,
            @QueryParam("firstname") String firstname,
            @QueryParam("lastname") String lastname,
            @QueryParam("gender") String gender) {
        CompetitionWebService ws = new CompetitionWebService();
        GetRunnerListRequest request = new GetRunnerListRequest();
        request.setToken(token);
        request.setRequestID(requestID);
        
        RunnerSearchType runnerSearchType = new RunnerSearchType();
        runnerSearchType.setFistname(firstname);
        runnerSearchType.setLastname(lastname);
        if (gender != null) runnerSearchType.setGender(GenderType.valueOf(gender));
        request.setSearch(runnerSearchType);
        return ws.getRunnerList(request);
    }

    @GET
    @Path("{id : \\d+}") //supports digits only
    @Produces(MediaType.APPLICATION_JSON)
    public RunnerType getRunner(@PathParam("id") String id,
            @QueryParam("token") String token,
            @QueryParam("rid") String requestID) {
        CompetitionWebService ws = new CompetitionWebService();
        GetRunnerRequest request = new GetRunnerRequest();
        request.setId(new BigInteger(id));
        request.setToken(token);
        request.setRequestID(requestID);
        
        RunnerType runnerType = ws.getRunner(request);
        return runnerType;
    }
    
    
    /**
     * POST method for updating or creating an instance of RunnersResource
     * @param content representation for the resource
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RunnerType addRunner(RunnerType content, 
                                @QueryParam("token") String token,
                                @QueryParam("rid") String requestID) throws JAXBException {
        CompetitionWebService ws = new CompetitionWebService();
        AddRunnerRequest request = new AddRunnerRequest();
        request.setFirstname(content.getFirstname());
        request.setLastname(content.getLastname());
        request.setGender(content.getGender());
        request.setBirthdate(content.getBirthdate());
        request.getBirthdate().setTimezone(DatatypeConstants.FIELD_UNDEFINED);
        request.setEmail(content.getEmail());
        request.setPhone(content.getPhone());
        request.setToken(token);
        request.setRequestID(requestID);
        return ws.addRunner(request);
    }
}
