/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.competition;

import com.sun.xml.ws.developer.SchemaValidation;
import ee.ttu.idu0075.database.Database;
import ee.ttu.idu0075.iapb155619.ws.competition.*;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Siim
 */
@SchemaValidation
@WebService(serviceName = "CompetitionService", portName = "CompetitionPort", endpointInterface = "ee.ttu.idu0075.iapb155619.ws.competition.CompetitionPortType", targetNamespace = "http://www.ttu.ee/idu0075/iapb155619/ws/competition", wsdlLocation = "WEB-INF/wsdl/CompetitionWebService/CompetitionService.wsdl")
public class CompetitionWebService {
    
    private static final String TOKEN = "secret";

    public RunnerType getRunner(GetRunnerRequest request) {
        if (request.getToken().equals(TOKEN)) {
            String response = Database.getResponse(request.getRequestID());
            if (response != null) {
                return getResponse(RunnerType.class, response);
            }
            
            RunnerType rt = Database.getRunner(request.getId().longValue(), request.getRequestID());
            
            return rt;
        }
        return null;
    }
    
    public <T> T getResponse(Class<T> className, String xmlString) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(className);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            T response = (T) jaxbUnmarshaller.unmarshal(new StringReader(xmlString));
            return response;
        } catch (JAXBException ex) {
            Logger.getLogger(CompetitionWebService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public RunnerType addRunner(AddRunnerRequest request) throws JAXBException {
        if (request.getToken().equals(TOKEN)) {
            String response = Database.getResponse(request.getRequestID());
            if (response != null) {
                return getResponse(RunnerType.class, response);
            }
            
            RunnerType rt = new RunnerType();
            
            rt.setFirstname(request.getFirstname());
            rt.setLastname(request.getLastname());
            rt.setGender(request.getGender());
            rt.setBirthdate(request.getBirthdate());
            rt.setEmail(request.getEmail());
            rt.setPhone(request.getPhone());
            
            rt = Database.addRunner(rt, request.getRequestID());
            
            return rt;
        }
        return null;
    }

    public GetRunnerListResponse getRunnerList(GetRunnerListRequest request) {
        if (request.getToken().equals(TOKEN)) {
            String response = Database.getResponse(request.getRequestID());
            if (response != null) {
                return getResponse(GetRunnerListResponse.class, response);
            }
            
            GetRunnerListResponse grlr = Database.getRunnerListResponse(request.getSearch(), request.getRequestID());
            
            return grlr;
        }
        return null;
    }

    public CompetitionType getCompetition(GetCompetitionRequest request) {
        if (request.getToken().equals(TOKEN)) {
            String response = Database.getResponse(request.getRequestID());
            if (response != null) {
                return getResponse(CompetitionType.class, response);
            }
            
            CompetitionType ct = Database.getCompetition(request.getId().longValue(), request.getRequestID());
            
            return ct;
        }
        return null;
    }

    public CompetitionType addCompetition(AddCompetitionRequest request) {
        if (request.getToken().equals(TOKEN)) {
            String response = Database.getResponse(request.getRequestID());
            if (response != null) {
                return getResponse(CompetitionType.class, response);
            }
            
            CompetitionType ct = new CompetitionType();
            
            ct.setCompetitionName(request.getCompetitionName());
            ct.setCompetitionDate(request.getCompetitionDate());
            ct.setRegistrationDate(request.getRegistrationDate());
            ct.setCompetitionRunnerList(new CompetitionRunnerListType());
            
            ct = Database.addCompetition(ct, request.getRequestID());
            
            return ct;
        }
        return null;
    }

    public GetCompetitionListResponse getCompetitionList(GetCompetitionListRequest request) {
        if (request.getToken().equals(TOKEN)) {
            String response = Database.getResponse(request.getRequestID());
            if (response != null) {
                return getResponse(GetCompetitionListResponse.class, response);
            }
            
            GetCompetitionListResponse gclr = Database.getCompetitionListResponse(request.getSearch(), request.getRequestID());
            
            return gclr;
        }
        return null;
    }

    public CompetitionRunnerListType getCompetitionRunnerList(GetCompetitionRunnerListRequest request) {
        if (request.getToken().equals(TOKEN)) {
            String response = Database.getResponse(request.getRequestID());
            if (response != null) {
                return getResponse(CompetitionRunnerListType.class, response);
            }
            
            CompetitionRunnerListType crlt = Database.getCompetitionRunnerListResponse(request.getCompetitionId().longValue(), request.getSearch(), request.getRequestID());
            
            return crlt;
        }
        return null;
    }

    public CompetitionRunnerType addCompetitionRunner(AddCompetitionRunnerRequest request) {
        if (request.getToken().equals(TOKEN)) {
            String response = Database.getResponse(request.getRequestID());
            if (response != null) {
                return getResponse(CompetitionRunnerType.class, response);
            }
            
            CompetitionRunnerType crt = new CompetitionRunnerType();
            
            crt.setGroup(request.getGroup());
            crt.setNumber(request.getNumber());
            
            crt = Database.addCompetitionRunner(crt, request.getRequestID(), request.getCompetitionId(), request.getRunnerId());
            
            return crt;
        }
        return null;
    }
    
}
