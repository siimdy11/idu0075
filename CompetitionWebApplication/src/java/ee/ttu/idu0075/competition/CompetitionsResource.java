/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.competition;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import ee.ttu.idu0075.iapb155619.ws.competition.AddCompetitionRequest;
import ee.ttu.idu0075.iapb155619.ws.competition.AddCompetitionRunnerRequest;
import ee.ttu.idu0075.iapb155619.ws.competition.AddRunnerRequest;
import ee.ttu.idu0075.iapb155619.ws.competition.CompetitionRunnerListType;
import ee.ttu.idu0075.iapb155619.ws.competition.CompetitionRunnerSearchType;
import ee.ttu.idu0075.iapb155619.ws.competition.CompetitionRunnerType;
import ee.ttu.idu0075.iapb155619.ws.competition.CompetitionSearchType;
import ee.ttu.idu0075.iapb155619.ws.competition.CompetitionType;
import ee.ttu.idu0075.iapb155619.ws.competition.GenderType;
import ee.ttu.idu0075.iapb155619.ws.competition.GetCompetitionListRequest;
import ee.ttu.idu0075.iapb155619.ws.competition.GetCompetitionListResponse;
import ee.ttu.idu0075.iapb155619.ws.competition.GetCompetitionRequest;
import ee.ttu.idu0075.iapb155619.ws.competition.GetCompetitionRunnerListRequest;
import ee.ttu.idu0075.iapb155619.ws.competition.RunnerType;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * REST Web Service
 *
 * @author Siim
 */
@Path("competitions")
public class CompetitionsResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of CompetitionsResource
     */
    public CompetitionsResource() {
    }

    /**
     * Retrieves representation of an instance of ee.ttu.idu0075.competition.CompetitionsResource
     * @return an instance of ee.ttu.idu0075.iapb155619.ws.competition.CompetitionType
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public GetCompetitionListResponse getCompetitionList(@QueryParam("token") String token,
            @QueryParam("rid") String requestID,
            @QueryParam("competition-name") String competitionName,
            @QueryParam("from-date") String fromDate,
            @QueryParam("to-date") String toDate,
            @QueryParam("registration-is-open") String registrationIsOpen,
            @QueryParam("competition-is-over") String competitionIsOver) {
        CompetitionWebService ws = new CompetitionWebService();
        GetCompetitionListRequest request = new GetCompetitionListRequest();
        request.setToken(token);
        request.setRequestID(requestID);
        
        CompetitionSearchType searchType = new CompetitionSearchType();
        searchType.setCompetitionName(competitionName);
        if (fromDate != null) searchType.setFromDate(XMLGregorianCalendarImpl.parse(fromDate));
        if (toDate != null) searchType.setToDate(XMLGregorianCalendarImpl.parse(toDate));
        if (registrationIsOpen != null) searchType.setRegistrationIsOpen(Boolean.valueOf(registrationIsOpen));
        if (competitionIsOver != null) searchType.setCompetitionIsOver(Boolean.valueOf(competitionIsOver));
        
        request.setSearch(searchType);
        
        return ws.getCompetitionList(request);
    }
    
    @GET
    @Path("{id : \\d+}") //supports digits only
    @Produces(MediaType.APPLICATION_JSON)
    public CompetitionType getCompetition(@PathParam("id") String id,
            @QueryParam("token") String token,
            @QueryParam("rid") String requestID) {
        CompetitionWebService ws = new CompetitionWebService();
        GetCompetitionRequest request = new GetCompetitionRequest();
        request.setId(new BigInteger(id));
        request.setToken(token);
        request.setRequestID(requestID);
        return ws.getCompetition(request);
    }

    /**
     * POST method for updating or creating an instance of RunnersResource
     * @param content representation for the resource
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public CompetitionType addCompetition(CompetitionType content, 
                                @QueryParam("token") String token,
                                @QueryParam("rid") String requestID) throws JAXBException {
        CompetitionWebService ws = new CompetitionWebService();
        AddCompetitionRequest request = new AddCompetitionRequest();
        request.setCompetitionName(content.getCompetitionName());
        request.setCompetitionDate(content.getCompetitionDate());
        request.getCompetitionDate().setTimezone(DatatypeConstants.FIELD_UNDEFINED);
        request.setRegistrationDate(content.getRegistrationDate());
        request.getRegistrationDate().setTimezone(DatatypeConstants.FIELD_UNDEFINED);
        request.setToken(token);
        request.setRequestID(requestID);
        return ws.addCompetition(request);
    }
    
    @GET
    @Path("{id : \\d+}/runners") //support digit only
    @Produces(MediaType.APPLICATION_JSON)
    public CompetitionRunnerListType getCompetitionRunnerList(@PathParam("id") String id,
            @QueryParam("token") String token,
            @QueryParam("rid") String requestID,
            @QueryParam("firstname") String firstname,
            @QueryParam("lastname") String lastname,
            @QueryParam("gender") String gender,
            @QueryParam("group") String group,
            @QueryParam("number") String number) {
        CompetitionWebService ws = new CompetitionWebService();
        GetCompetitionRunnerListRequest request = new GetCompetitionRunnerListRequest();
        request.setCompetitionId(new BigInteger(id));
        request.setToken(token);
        request.setRequestID(requestID);
        
        CompetitionRunnerSearchType searchType = new CompetitionRunnerSearchType();
        searchType.setFistname(firstname);
        searchType.setLastname(lastname);
        if (gender != null) searchType.setGender(GenderType.valueOf(gender));
        searchType.setGroup(group);
        searchType.setNumber(new BigInteger(number));
        
        request.setSearch(searchType);
        
        return ws.getCompetitionRunnerList(request);
    }
    
    @POST
    @Path("{id : \\d+}/runners") //support digit only
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public CompetitionRunnerType addCompetitionRunner(AddCompetitionRunnerRequest content, 
                                @QueryParam("token") String token,
                                @PathParam("id") String id,
                                @QueryParam("rid") String requestID) {
        CompetitionWebService ws = new CompetitionWebService();
        AddCompetitionRunnerRequest request = new AddCompetitionRunnerRequest();
        request.setCompetitionId(new BigInteger(id));
        request.setGroup(content.getGroup());
        request.setNumber(content.getNumber());
        request.setRunnerId(content.getRunnerId());
        request.setToken(token);
        request.setRequestID(requestID);
        return ws.addCompetitionRunner(request);    
    }
}
