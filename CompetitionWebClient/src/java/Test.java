
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import ee.ttu.idu0075.iapb155619.ws.competition.*;
import java.math.BigInteger;
import java.security.SecureRandom;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Siim
 */
public class Test {
    
    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();
    
    private static final String TOKEN = "secret";

    static String randomString(int len){
        StringBuilder sb = new StringBuilder(len);
        for(int i = 0; i < len; i++) 
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }
    
    public static void main(String[] args) {
        AddRunnerRequest addRunnerRequest = new AddRunnerRequest();
        addRunnerRequest.setFirstname("Anna");
        addRunnerRequest.setLastname("Kivi");
        addRunnerRequest.setGender(GenderType.M);
        addRunnerRequest.setBirthdate(XMLGregorianCalendarImpl.parse("1994-01-22"));
        addRunnerRequest.setEmail("anna.kivi@mail.ee");
        addRunnerRequest.setPhone("534234552");
        addRunnerRequest.setToken(TOKEN);
        addRunnerRequest.setRequestID(randomString(20));
        
        RunnerType runnerType = addRunner(addRunnerRequest);
        
        BigInteger id = runnerType.getId();
        
        
        GetRunnerRequest getRunnerRequest = new GetRunnerRequest();
        getRunnerRequest.setId(id);
        getRunnerRequest.setToken(TOKEN);
        getRunnerRequest.setRequestID(randomString(20));
        
        RunnerType rt = getRunner(getRunnerRequest);
        
        System.out.println(rt.getId());
        System.out.println(rt.getFirstname());
        System.out.println(rt.getLastname());
    }

    private static RunnerType addRunner(AddRunnerRequest parameter) {
        CompetitionService service = new ee.ttu.idu0075.iapb155619.ws.competition.CompetitionService();
        CompetitionPortType port = service.getCompetitionPort();
        return port.addRunner(parameter);
    }

    private static RunnerType getRunner(GetRunnerRequest parameter) {
        CompetitionService service = new CompetitionService();
        CompetitionPortType port = service.getCompetitionPort();
        return port.getRunner(parameter);
    }
    
    
    
}
