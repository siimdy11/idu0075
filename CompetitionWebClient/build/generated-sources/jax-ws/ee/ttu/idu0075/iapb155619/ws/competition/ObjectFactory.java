
package ee.ttu.idu0075.iapb155619.ws.competition;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ee.ttu.idu0075.iapb155619.ws.competition package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetRunnerResponse_QNAME = new QName("http://www.ttu.ee/idu0075/iapb155619/ws/competition", "getRunnerResponse");
    private final static QName _AddRunnerResponse_QNAME = new QName("http://www.ttu.ee/idu0075/iapb155619/ws/competition", "addRunnerResponse");
    private final static QName _GetCompetitionResponse_QNAME = new QName("http://www.ttu.ee/idu0075/iapb155619/ws/competition", "getCompetitionResponse");
    private final static QName _AddCompetitionResponse_QNAME = new QName("http://www.ttu.ee/idu0075/iapb155619/ws/competition", "addCompetitionResponse");
    private final static QName _GetCompetitionRunnerListResponse_QNAME = new QName("http://www.ttu.ee/idu0075/iapb155619/ws/competition", "getCompetitionRunnerListResponse");
    private final static QName _AddCompetitionRunnerResponse_QNAME = new QName("http://www.ttu.ee/idu0075/iapb155619/ws/competition", "addCompetitionRunnerResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ee.ttu.idu0075.iapb155619.ws.competition
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetRunnerRequest }
     * 
     */
    public GetRunnerRequest createGetRunnerRequest() {
        return new GetRunnerRequest();
    }

    /**
     * Create an instance of {@link RunnerType }
     * 
     */
    public RunnerType createRunnerType() {
        return new RunnerType();
    }

    /**
     * Create an instance of {@link AddRunnerRequest }
     * 
     */
    public AddRunnerRequest createAddRunnerRequest() {
        return new AddRunnerRequest();
    }

    /**
     * Create an instance of {@link GetRunnerListRequest }
     * 
     */
    public GetRunnerListRequest createGetRunnerListRequest() {
        return new GetRunnerListRequest();
    }

    /**
     * Create an instance of {@link RunnerSearchType }
     * 
     */
    public RunnerSearchType createRunnerSearchType() {
        return new RunnerSearchType();
    }

    /**
     * Create an instance of {@link GetRunnerListResponse }
     * 
     */
    public GetRunnerListResponse createGetRunnerListResponse() {
        return new GetRunnerListResponse();
    }

    /**
     * Create an instance of {@link GetCompetitionRequest }
     * 
     */
    public GetCompetitionRequest createGetCompetitionRequest() {
        return new GetCompetitionRequest();
    }

    /**
     * Create an instance of {@link CompetitionType }
     * 
     */
    public CompetitionType createCompetitionType() {
        return new CompetitionType();
    }

    /**
     * Create an instance of {@link AddCompetitionRequest }
     * 
     */
    public AddCompetitionRequest createAddCompetitionRequest() {
        return new AddCompetitionRequest();
    }

    /**
     * Create an instance of {@link GetCompetitionListRequest }
     * 
     */
    public GetCompetitionListRequest createGetCompetitionListRequest() {
        return new GetCompetitionListRequest();
    }

    /**
     * Create an instance of {@link CompetitionSearchType }
     * 
     */
    public CompetitionSearchType createCompetitionSearchType() {
        return new CompetitionSearchType();
    }

    /**
     * Create an instance of {@link GetCompetitionListResponse }
     * 
     */
    public GetCompetitionListResponse createGetCompetitionListResponse() {
        return new GetCompetitionListResponse();
    }

    /**
     * Create an instance of {@link GetCompetitionRunnerListRequest }
     * 
     */
    public GetCompetitionRunnerListRequest createGetCompetitionRunnerListRequest() {
        return new GetCompetitionRunnerListRequest();
    }

    /**
     * Create an instance of {@link CompetitionRunnerSearchType }
     * 
     */
    public CompetitionRunnerSearchType createCompetitionRunnerSearchType() {
        return new CompetitionRunnerSearchType();
    }

    /**
     * Create an instance of {@link CompetitionRunnerListType }
     * 
     */
    public CompetitionRunnerListType createCompetitionRunnerListType() {
        return new CompetitionRunnerListType();
    }

    /**
     * Create an instance of {@link AddCompetitionRunnerRequest }
     * 
     */
    public AddCompetitionRunnerRequest createAddCompetitionRunnerRequest() {
        return new AddCompetitionRunnerRequest();
    }

    /**
     * Create an instance of {@link CompetitionRunnerType }
     * 
     */
    public CompetitionRunnerType createCompetitionRunnerType() {
        return new CompetitionRunnerType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RunnerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/iapb155619/ws/competition", name = "getRunnerResponse")
    public JAXBElement<RunnerType> createGetRunnerResponse(RunnerType value) {
        return new JAXBElement<RunnerType>(_GetRunnerResponse_QNAME, RunnerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RunnerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/iapb155619/ws/competition", name = "addRunnerResponse")
    public JAXBElement<RunnerType> createAddRunnerResponse(RunnerType value) {
        return new JAXBElement<RunnerType>(_AddRunnerResponse_QNAME, RunnerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompetitionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/iapb155619/ws/competition", name = "getCompetitionResponse")
    public JAXBElement<CompetitionType> createGetCompetitionResponse(CompetitionType value) {
        return new JAXBElement<CompetitionType>(_GetCompetitionResponse_QNAME, CompetitionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompetitionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/iapb155619/ws/competition", name = "addCompetitionResponse")
    public JAXBElement<CompetitionType> createAddCompetitionResponse(CompetitionType value) {
        return new JAXBElement<CompetitionType>(_AddCompetitionResponse_QNAME, CompetitionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompetitionRunnerListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/iapb155619/ws/competition", name = "getCompetitionRunnerListResponse")
    public JAXBElement<CompetitionRunnerListType> createGetCompetitionRunnerListResponse(CompetitionRunnerListType value) {
        return new JAXBElement<CompetitionRunnerListType>(_GetCompetitionRunnerListResponse_QNAME, CompetitionRunnerListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompetitionRunnerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/iapb155619/ws/competition", name = "addCompetitionRunnerResponse")
    public JAXBElement<CompetitionRunnerType> createAddCompetitionRunnerResponse(CompetitionRunnerType value) {
        return new JAXBElement<CompetitionRunnerType>(_AddCompetitionRunnerResponse_QNAME, CompetitionRunnerType.class, null, value);
    }

}
