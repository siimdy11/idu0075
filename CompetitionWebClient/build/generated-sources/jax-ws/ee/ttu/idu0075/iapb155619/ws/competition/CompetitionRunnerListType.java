
package ee.ttu.idu0075.iapb155619.ws.competition;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for competitionRunnerListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="competitionRunnerListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="competitionRunner" type="{http://www.ttu.ee/idu0075/iapb155619/ws/competition}competitionRunnerType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "competitionRunnerListType", propOrder = {
    "competitionRunner"
})
public class CompetitionRunnerListType {

    protected List<CompetitionRunnerType> competitionRunner;

    /**
     * Gets the value of the competitionRunner property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the competitionRunner property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCompetitionRunner().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CompetitionRunnerType }
     * 
     * 
     */
    public List<CompetitionRunnerType> getCompetitionRunner() {
        if (competitionRunner == null) {
            competitionRunner = new ArrayList<CompetitionRunnerType>();
        }
        return this.competitionRunner;
    }

}
